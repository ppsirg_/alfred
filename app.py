import botogram
from settings import TOKEN
from examples.back import search_song
bot = botogram.create(TOKEN)

@bot.command("hello")
def hello_command(chat, message, args):
    """Say hello to the world!"""
    chat.send("Hello world")

@bot.command('search')
def do_query(chat, message, args):
    found_songs = search_song(message[7:])
    chat.send(found_songs)


if __name__ == "__main__":
    bot.run()
