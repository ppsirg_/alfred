# alfred - bot framework for applications

#summary

an extensible bot backend to have chatbots compatibles with different
chat interfaces

#functionalities

- receive and response users on:
    - facebook messenger
    - telegram
    - line
    - slack

#how to help

please follow [pep8 standar](https://www.python.org/dev/peps/pep-0008/),
use docstrings and if possible do tests, also on commits, enunciate dependencies
of your code

we accept contributions, ideas or bugs please use issue tracker, we will tag as
**easy** the ones that can be done by newbies or the ones to get familiar with
code

- new functionalities: fork this repository and send a pull request when you're
  done, we require at least docstrings documentation
- bugfixes: fork this repository and send a pull request with the link of the
  issue solved by the pull request

#contact us

please contact us in our [facebook page](https://www.facebook.com/pythonistapopayan)
for more information or to get involved
