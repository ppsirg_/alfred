"""
implementacion de alto nivel de bot de telegram

usa la libreria botogram https://botogram.pietroalbini.org/
"""

import botogram
from settings import TOKEN
from examples.back import search_song, get_song_lyrics
bot = botogram.create(TOKEN)

@bot.command("hello")
def hello_command(chat, message, args):
    """Say hello to the world!"""
    chat.send("Hello world")

@bot.command('search')
def do_query(chat, message, args):
    found_songs = search_song(message)
    chat.send(found_songs)

@bot.command('lyrics')
def get_lyrics(chat, message, args):
    lyrics_text = get_song_lyrics(message)
    chat.send(lyrics_text)


if __name__ == "__main__":
    bot.run()
