#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
implementacion de bajo nivel de bot en telegram

usa la libreria python-telegram-bot
https://github.com/python-telegram-bot/python-telegram-bot
"""
# expresiones regulares
import re
# libreria para manejar telegram
import telegram
# para manejar fechas y horas
from datetime import datetime
from time import time
# archivo con las credenciales de genius y telegram bot
from credentials import TOKEN
# para generar tiempos sin ejecucion de codigo
from time import sleep
# logica del bot
from back import get_song_lyrics, search_song


class bot_manager(object):
    'representa el objeto que va a manejar el bot'

    def __init__(self):
        'incializador de variables del objeto bot_manager'
        self.bot = telegram.Bot(token=TOKEN['telegram']['api_token'])
        self.stop = False
        self.updates_historial = []
        self.orders = {
            'hello': (  # nombre de la funcion de saludar
                re.compile('[hello|hola].*'),  # expresion regular para reconocer un saludo del usuario
                self.say_hello  # funcion que responde al saludo del usuario
            ),
            'search_song': (  #buscar una cancion
                re.compile('search .+'),
                self.search_lyrics
            ),
            'get_song_lyrics': (  # obtener la letra de una cacion
                re.compile('lyrics of .+'),
                self.get_lyrics
            ),
            }
        # detecta la fecha a la que empieza a funcionar el programa
        self.start_operation_date = time()
        # guarda las id de las actualizaciones de telegram que
        # han sido procesadas
        self.procesed_orders = []

    def say_hello(self, update):
        'saludar al usuario'
        update.message.reply_text(
            'hi there buddy!!'
        )

    def get_lyrics(self, update):
        'bajar la letra de una cancion de genius'
        song_id = update.to_dict()['message']['text'].lower()
        # song_id = 'lyrics of 2423176'
        song_id = song_id[10:]
        # song_id = '2423176'
        lyrics = get_song_lyrics(song_id)
        # responda a la actualizacion
        update.message.reply_text(lyrics)

    def search_lyrics(self, update):
        'buscar una cancion en genius'
        query_text = update.to_dict()['message']['text'].lower()
        # query_text = 'search run or hide'
        query_text = query_text[7:]
        # query_text = 'run or hide'
        found_songs = search_song(query_text)
        # responda a la actualizacion
        update.message.reply_text(found_songs)

    def process_updates(self, update):
        'procesa las actualizaciones de telegram'
        # obtiene la id de la actualizacion de telegram
        id_ = update.update_id
        # si la id esta en ordenes procesadas, no la procese de nuevo
        # si no esta, procesela
        if id_ not in self.procesed_orders:
            # obtener el texto y la fecha de la actualizacion que se
            # esta procesando
            u = update.to_dict()
            text = u['message']['text'].lower()
            date = u['message']['date']
            # si la fecha de la actualizacion es posterior a la fecha
            # de inicio del servidor (guardada en self.start_operation_date)
            # entonces procese la actualizacion, si no, ignorela
            if int(date) >= self.start_operation_date:
                for o in self.orders:
                    print(
                        '[{d.update_id}]{d.message.text}'.format(
                            d=update
                        )
                    )
                    print(text)
                    # si el texto de la actualizacion es detectado como
                    # valido por los datos, entonces ejecute la funcion
                    # definida para el mensaje en self.orders
                    if self.orders[o][0].match(text):
                        print(
                            '[processing]: action {}'.format(o)
                        )
                        self.orders[o][1](update)
                        # termine la verificación de otras funciones si
                        # ya se proceso una
                        break
            # agregue la id de la actualizacion de telegram a la lista
            # de las actualizaciones procesadas
            self.procesed_orders.append(id_)

    def main_loop(self):
        'ciclo principal del bot de telegram'
        while not self.stop:
            # obtener actualizaciones de telegram
            updates = self.bot.get_updates()
            print('[main_loop]: check for updates')
            # procesar cada actualización obtenida
            for u in updates:
                self.process_updates(u)
            # esperar 5 segundos hasta volver a pedir
            # actualizaciones a telegram
            sleep(5)


if __name__ == '__main__':
    # crear un manejador de bot
    b = bot_manager()
    # inicial el ciclo principal del manejador de bot
    b.main_loop()
