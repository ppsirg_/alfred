import requests
from bs4 import BeautifulSoup
from credentials import TOKEN

headers = {'Authorization': 'Bearer {}'.format(TOKEN['genius']['access_token'])}
root_url = 'https://api.genius.com'


def search_song(query_input):
    'search a song in genius api'
    search_url = '/'.join([root_url, 'search'])
    payload = {'q': query_input}
    response = requests.get(search_url, headers=headers, params=payload)
    r = response.json()
    songs = ['songs found are:\n']
    try:
        for h in r['response']['hits']:
            title = h['result']['full_title']
            _id = h['result']['id']
            songs.append('- [{}]: {}'.format(_id, title))
    except Exception as e:
        songs.append('no match, there is an error')
        songs.append(e)
    song_results = '\n'.join(songs)
    return song_results


def get_song_lyrics(song_id):
    'get song lyrics with genius api'
    song_id_url = '/'.join([root_url, 'songs', song_id])
    response = requests.get(song_id_url, headers=headers)
    r = response.json()
    song_url = r['response']['song']['url']
    song_name = r['response']['song']['full_title']
    r = requests.get(song_url)
    soup = BeautifulSoup(r.text, 'html.parser')
    tag = soup.lyrics
    lyrics = tag.get_text()
    lyrics_text = song_name + lyrics
    return lyrics_text


if __name__ == '__main__':
    get_song_lyrics('2423176')
