
##general resources

- [how to create a chatbot](https://apps.worldwritable.com/tutorials/chatbot/)
- [resources about bots](https://www.fullstackpython.com/bots.html)


##python libraries:

- [botogram, framework for telegram bots](https://botogram.pietroalbini.org/docs/0.3/)
- [errbot, chatbot library](http://errbot.io/en/latest/)
- [chatterbot, machine learning, conversational dialog engine for creating chat bots](https://github.com/gunthercox/ChatterBot)
- [bottoku, chatbot microframework](https://pypi.python.org/pypi/bottoku/0.1.6)
